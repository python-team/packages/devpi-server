Metadata-Version: 1.1
Name: devpi-server
Version: 4.4.0
Summary: devpi-server: reliable private and pypi.python.org caching server
Home-page: http://doc.devpi.net
Author: Holger Krekel, Florian Schulze
Author-email: holger@merlinux.eu
License: MIT
Description-Content-Type: UNKNOWN
Description: =============================================================================
        devpi-server: pypi server for caching and private indexes
        =============================================================================
        
        * `issue tracker <https://github.com/devpi/devpi/issues>`_, `repo
          <https://github.com/devpi/devpi>`_
        
        * IRC: #devpi on freenode, `mailing list
          <https://mail.python.org/mm3/mailman3/lists/devpi-dev.python.org/>`_ 
        
        * compatibility: {win,unix}-py{27,34,35,36,py}
        
        consistent robust pypi-cache
        ============================
        
        You can point ``pip or easy_install`` to the ``root/pypi/+simple/``
        index, serving as a self-updating transparent cache for pypi-hosted
        **and** external packages.  Cache-invalidation uses the latest and
        greatest PyPI protocols.  The cache index continues to serve when
        offline and will resume cache-updates once network is available.
        
        user specific indexes
        =====================
        
        Each user (which can represent a person or a project, team) can have
        multiple indexes and upload packages and docs via standard ``setup.py``
        invocations command.  Users and indexes can be manipulated through a
        RESTful HTTP API.
        
        index inheritance
        =================
        
        Each index can be configured to merge in other indexes so that it serves
        both its uploads and all releases from other index(es).  For example, an
        index using ``root/pypi`` as a parent is a good place to test out a
        release candidate before you push it to PyPI.
        
        good defaults and easy deployment
        =================================
        
        Get started easily and create a permanent devpi-server deployment
        including pre-configured templates for ``nginx`` and cron. 
        
        separate tool for Packaging/Testing activities
        ==============================================
        
        The complementary `devpi-client <http://pypi.python.org/devpi-client>`_ tool
        helps to manage users, indexes, logins and typical setup.py-based upload and
        installation workflows.
        
        See http://doc.devpi.net for getting started and documentation.
        
        
        
        =========
        Changelog
        =========
        
        
        
        .. towncrier release notes start
        
        4.4.0 (2018-01-17)
        ==================
        
        Features
        --------
        
        - fix issue469: Add ``acl_toxresult_upload`` to index configuration.
        
        
        Bug Fixes
        ---------
        
        - fix issue477: importing with --no-root-pypi failed.
        
        
        4.3.2 (2017-12-19)
        ==================
        
        Bug Fixes
        ---------
        
        - fix issue480: store additional metadata for packages.
        
        - fix issue489: ignore download errors in replica thread for mirrored files
          that were removed from the filesystem on master and from the mirrored source.
        
        
        4.3.1 (2017-11-23)
        ==================
        
        Bug Fixes
        ---------
        
        - fix +api on replica when master is down.
        
        
        4.3.1rc1 (2017-09-08)
        =====================
        
        Bug Fixes
        ---------
        
        - fix issue345: remove ``expires -1`` option in example nginx proxy config for
          devpi. When there are no ``Expires`` and ``Cache-Control`` headers, then pip
          does not cache the simple pages, the headers set by ``expires -1`` caused pip
          to cache for 5 minutes.
        
        - fix issue402: the redirect to simple index didn't take X-Outside-Url into
          account.
        
        - fix for url decoding issue with mirrors. When package filenames contain
          characters such as `!` or `+`, these get URL encoded to `%21` and `%2B` in
          the remote simple index. This fix ensures that in the filename saved to the
          disk cache these are decoded back to `!` or `+`.
        
        - fix issue434: ``--status`` didn't work anymore. The background server
          functionality is now deprecated, see --gen-config to use a process manager
          from your OS.
        
        - fix issue449: push to pypi broke again due to a changed reply.
        
        - fix remote file url for mirrors not named "root/pypi" which provide file
          hashes.
        
        - fix issue401: fix traceback and inaccessible index by ignoring removed bases.
        
        
        4.3.0 (2017-04-23)
        ==================
        
        - allow upload of documentation without first registering the project or
          uploading releases.
        
        - add a new command line option ``--replica-max-retries``
        
          Under certain network conditions, it's possible for a connection from devpi
          to replicas (such as pypi) to be dropped, resulting in a 502 bad gateway
          being returned from devpi. When replica-max-retries is set to a number > 0,
          devpi will attempt to retry connections until the retry limit is reached.
        
        - fix ``--import`` after ``--init`` option was added.
        
        - fix import when the export contains a bases cycle.
        
        - fix issue350: use absolute path to devpi-server when starting
          background process.
        
        - fix issue392: setting user password from command line when password hash
          wasn't migrated yet failed.
        
        - fix #381: indicate acceptable exit status for systemd.
        
        - remove broken ``--bypass-cdn`` option.
        
        
Keywords: pypi realtime cache server
Platform: UNKNOWN
Classifier: Development Status :: 5 - Production/Stable
Classifier: Environment :: Web Environment
Classifier: Intended Audience :: Developers
Classifier: Intended Audience :: System Administrators
Classifier: License :: OSI Approved :: MIT License
Classifier: Programming Language :: Python
Classifier: Topic :: Internet :: WWW/HTTP
Classifier: Topic :: Internet :: WWW/HTTP :: WSGI :: Application
Classifier: Programming Language :: Python :: Implementation :: PyPy
Classifier: Programming Language :: Python :: 2.7
Classifier: Programming Language :: Python :: 3.4
Classifier: Programming Language :: Python :: 3.5
Classifier: Programming Language :: Python :: 3.6
